#include<iostream>
#include<ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <sensor_msgs/NavSatStatus.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>

using namespace std;
class gpsXmap{
	public:
		void mapXutm_callback(const nav_msgs::OdometryConstPtr& mapXutm_odom);	
		void gpsXutm_callback(const nav_msgs::OdometryConstPtr& mapXutm_odom);	
		void update(){
			gpsXmap_odom_pub.publish(gpsXmap_odom);	
		};
		void subscribe(){
			mapXutm_odom_sub = node.subscribe("/mapXutm_odom", 10, &gpsXmap::mapXutm_callback, this);
			gpsXutm_odom_sub = node.subscribe("/gpsXutm_odom", 10, &gpsXmap::gpsXutm_callback, this);
			gpsXmap_odom_pub = node.advertise<nav_msgs::Odometry>("gpsXmap_odom", 1000);
		};

	

	private:
		ros::NodeHandle node;
		nav_msgs::Odometry gpsXmap_odom;
		nav_msgs::Odometry gpsXutm_odom;
		nav_msgs::Odometry p_mapXutm_odom;
		ros::Subscriber mapXutm_odom_sub;
		ros::Subscriber gpsXutm_odom_sub;
		ros::Publisher gpsXmap_odom_pub; 
};

void gpsXmap::mapXutm_callback(const nav_msgs::OdometryConstPtr& mapXutm_odom){
	p_mapXutm_odom.pose.pose.position.x = mapXutm_odom->pose.pose.position.x;
	p_mapXutm_odom.pose.pose.position.y = mapXutm_odom->pose.pose.position.y;
	p_mapXutm_odom.pose.pose.position.z = mapXutm_odom->pose.pose.position.z;
	p_mapXutm_odom.pose.pose.orientation.x = mapXutm_odom->pose.pose.orientation.x;
	p_mapXutm_odom.pose.pose.orientation.y = mapXutm_odom->pose.pose.orientation.y;
	p_mapXutm_odom.pose.pose.orientation.z = mapXutm_odom->pose.pose.orientation.z;
	p_mapXutm_odom.pose.pose.orientation.w = mapXutm_odom->pose.pose.orientation.w;
};


void gpsXmap::gpsXutm_callback(const nav_msgs::OdometryConstPtr& gpsXutm_odom){
	gpsXmap_odom.pose.pose.position.x = gpsXutm_odom->pose.pose.position.x - p_mapXutm_odom.pose.pose.position.x;
	gpsXmap_odom.pose.pose.position.y = gpsXutm_odom->pose.pose.position.y - p_mapXutm_odom.pose.pose.position.y;
	gpsXmap_odom.pose.pose.position.z = gpsXutm_odom->pose.pose.position.z - p_mapXutm_odom.pose.pose.position.z;
	gpsXmap_odom.pose.pose.orientation.x = gpsXutm_odom->pose.pose.orientation.x - p_mapXutm_odom.pose.pose.orientation.x;
	gpsXmap_odom.pose.pose.orientation.y = gpsXutm_odom->pose.pose.orientation.y - p_mapXutm_odom.pose.pose.orientation.y;
	gpsXmap_odom.pose.pose.orientation.z = gpsXutm_odom->pose.pose.orientation.z - p_mapXutm_odom.pose.pose.orientation.z;
	gpsXmap_odom.pose.pose.orientation.w = gpsXutm_odom->pose.pose.orientation.w - p_mapXutm_odom.pose.pose.orientation.w;
	gpsXmap_odom_pub.publish(gpsXmap_odom);
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "gps2map_node");
	ros::start();
	gpsXmap car;
	car.subscribe();
	car.update();
	ros::spin();
	
}
