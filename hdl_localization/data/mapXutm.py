#!/usr/bin/env python

import rospy
import os
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry

def started():
  Odom=Odometry()
  data1=rospy.wait_for_message('gpsXutm_odom', Odometry, timeout=5)
  data2=rospy.wait_for_message('IMU_data', Imu, timeout=5)
  Odom.header.frame_id = "utm"
  Odom.header.stamp = rospy.Time.now()
  Odom.child_frame_id = "map"
  Odom.pose.pose.position = data1.pose.pose.position
  Odom.pose.pose.orientation = data2.orientation
  while not rospy.is_shutdown():
    mapXutm_odom_pub.publish(Odom)

if __name__ == '__main__':
  rospy.init_node('mapXutm_publisher')
  mapXutm_odom_pub = rospy.Publisher('mapXutm_odom', Odometry, queue_size=10)
  started()

