#include<iostream>
#include<ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <sensor_msgs/NavSatStatus.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>

using namespace std;
class carXutm{
	public:
		void mapXutm_callback(const nav_msgs::OdometryConstPtr& mapXutm_odom);	
		void carXmap_callback(const nav_msgs::OdometryConstPtr& mapXutm_odom);	
		void update(){
			carXutm_odom_pub.publish(carXutm_odom);	
		};
		void subscribe(){
			mapXutm_odom_sub = node.subscribe("/mapXutm_odom", 10, &carXutm::mapXutm_callback, this);
			carXmap_odom_sub = node.subscribe("/carXmap_odom", 10, &carXutm::carXmap_callback, this);
			carXutm_odom_pub = node.advertise<nav_msgs::Odometry>("carXutm_odom", 1000);
		};

	

	private:
		ros::NodeHandle node;
		nav_msgs::Odometry carXutm_odom;
		nav_msgs::Odometry carXmap_odom;
		nav_msgs::Odometry p_mapXutm_odom;
		ros::Subscriber mapXutm_odom_sub;
		ros::Subscriber carXmap_odom_sub;
		ros::Publisher carXutm_odom_pub; 
};

void carXutm::mapXutm_callback(const nav_msgs::OdometryConstPtr& mapXutm_odom){
	p_mapXutm_odom.pose.pose.position.x = mapXutm_odom->pose.pose.position.x;
	p_mapXutm_odom.pose.pose.position.y = mapXutm_odom->pose.pose.position.y;
	p_mapXutm_odom.pose.pose.position.z = mapXutm_odom->pose.pose.position.z;
	p_mapXutm_odom.pose.pose.orientation.x = mapXutm_odom->pose.pose.orientation.x;
	p_mapXutm_odom.pose.pose.orientation.y = mapXutm_odom->pose.pose.orientation.y;
	p_mapXutm_odom.pose.pose.orientation.z = mapXutm_odom->pose.pose.orientation.z;
	p_mapXutm_odom.pose.pose.orientation.w = mapXutm_odom->pose.pose.orientation.w;
};


void carXutm::carXmap_callback(const nav_msgs::OdometryConstPtr& carXmap_odom){
	carXutm_odom.pose.pose.position.x = carXmap_odom->pose.pose.position.x + p_mapXutm_odom.pose.pose.position.x;
	carXutm_odom.pose.pose.position.y = carXmap_odom->pose.pose.position.y + p_mapXutm_odom.pose.pose.position.y;
	carXutm_odom.pose.pose.position.z = carXmap_odom->pose.pose.position.z + p_mapXutm_odom.pose.pose.position.z;
	carXutm_odom.pose.pose.orientation.x = carXmap_odom->pose.pose.orientation.x + p_mapXutm_odom.pose.pose.orientation.x;
	carXutm_odom.pose.pose.orientation.y = carXmap_odom->pose.pose.orientation.y + p_mapXutm_odom.pose.pose.orientation.y;
	carXutm_odom.pose.pose.orientation.z = carXmap_odom->pose.pose.orientation.z + p_mapXutm_odom.pose.pose.orientation.z;
	carXutm_odom.pose.pose.orientation.w = carXmap_odom->pose.pose.orientation.w + p_mapXutm_odom.pose.pose.orientation.w;
	carXutm_odom_pub.publish(carXutm_odom);
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "carXutm_node");
	ros::start();
	carXutm car;
	car.subscribe();
	car.update();
	ros::spin();
	
}
